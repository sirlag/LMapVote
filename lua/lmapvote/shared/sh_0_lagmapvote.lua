LMapVote = {}

LMapVote.Map = class("Map")
LMapVote.Map.static.DB = "LMapVote"
LMapVote.Map.static.model = {
    tableName = "lmv_maplist",
    fields = {
        mapName = "string",
        mapSize = "string",
        stars = "int",
        popularity = "int"
    }
}
LMapVote.Map:include(DatabaseModel)

