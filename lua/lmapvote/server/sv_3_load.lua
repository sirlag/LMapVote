
if LMapVote.MapLoader == "fs" then
	-- Use the filesystem. We will read the maps list from a file called "lmapvote.txt" in the "data" folder.

	if not file.Exists("lmapvote.txt","DATA") then
		file.Write("lmapvote.txt","gm_construct\ngm_flatgrass");
	end

	local maps = file.Read("lmapvote.txt","DATA");
	if not maps then
		Error("File I/O error; Coudldn't open vote list.");
		return
	end

	maps=string.lower(maps);
	maps=string.gsub(maps," ","_");
	maps=string.gsub(maps,".bsp","");
	maps=string.gsub(maps,".bz2","");
	maps=string.Explode("\n",maps);
    
    MsgC(Color(102,255,51),"LMAPVOTE HAS FOUND "..#maps.." MAPS\n")

	if not maps or not maps[1] then return end

	for k,v in ipairs(maps)do
		if string.lower(v) == string.gsub(string.lower(game.GetMap())," ","_") then
			table.remove(maps,k);
			break;
		end
	end

	if ( #maps > 8 and not LMapVote.AllowExtend ) or #maps > 7 then
		LMapVote.MapSelection = {};
		while(#LMapVote.MapSelection < 8)do
			local rnd=math.random(1,#maps);
			table.insert(LMapVote.MapSelection,maps[rnd]);
			table.remove(maps,rnd);
		end
	else
		LMapVote.MapSelection = maps;
	end

	if LMapVote.AllowExtend then
		LMapVote.MapSelection[#LMapVote.MapSelection+1]=string.lower(game.GetMap());
	end

	MsgC(Color(255,255,255,0),"\nThe following maps will be voted on later this game:\n");
	for k,v in pairs(LMapVote.MapSelection)do
		MsgC(Color(255,255,255,0),"\t"..k..".\t"..v);

		if file.Exists("materials/lmapvote/maps/"..v..".png","GAME") and not LMapVote.IconsURL then
			resource.AddSingleFile ("materials/lmapvote/maps/"..v..".png");
		end
	end
	
elseif LMapVote.MapLoader == "db" then
    size = ""
	if(#player.GetAll() < 7) then size = "small"
	elseif(#player.GetAll() < 11) then size = "medium"
	elseif(#player.Getall() < 16) then size = "large"
	else size = "massive"
	end
	
	local maps = {}
    LMapVote.Map.getDbEntries("WHERE mapSize = \""..size.."\"")
    :Then(function(allMaps)
        for _ , map in pairs(allMaps) do
            table.insert(maps, map.mapName)
        end
    end)
    
   MsgC(Color(102,255,51),"LMAPVOTE HAS FOUND "..#maps.." MAPS\n")
    
	if not maps or not maps[1] then return end

	for k,v in ipairs(maps)do
		if string.lower(v) == string.gsub(string.lower(game.GetMap())," ","_") then
			table.remove(maps,k);
			break;
		end
	end
    if ( #maps > 8 and not LMapVote.AllowExtend ) or #maps > 7 then
        LMapVote.MapSelection = {};
        while(#LMapVote.MapSelection < 8)do
            local rnd=math.random(1,#maps);
            table.insert(LMapVote.MapSelection,maps[rnd]);
            table.remove(maps,rnd);
        end
    else
        LMapVote.MapSelection = maps
    end
    
	if LMapVote.AllowExtend then
		LMapVote.MapSelection[#LMapVote.MapSelection+1]=string.lower(game.GetMap());
	end

	MsgC(Color(255,255,255,0),"\nThe following maps will be voted on later this game:\n");
	for k,v in pairs(LMapVote.MapSelection)do
		MsgC(Color(255,255,255,0),"\t"..k..".\t"..v);

		if file.Exists("materials/lmapvote/maps/"..v..".png","GAME") and not LMapVote.IconsURL then
			resource.AddSingleFile ("materials/lmapvote/maps/"..v..".png");
		end
	end
end