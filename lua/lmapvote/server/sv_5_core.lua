LMapVote.Votes = {}

function LMapVote:Start()
	if not LMapVote.MapSelection or #LMapVote.MapSelection < 1 then
		Error("Not enough maps selected to load mapvote; is mapvote file setup correctly?\n");
		return;
	end

	LMapVote.Votes = {}

	net.Start("LMapVote.OpenMapVote");
	net.WriteTable(LMapVote.MapSelection);
	net.Broadcast();

	timer.Simple(LMapVote.VoteTime,function()
		LMapVote:Stop();
	end);

	LMapVote._busy=true;
end

function LMapVote:Stop()
	if not LMapVote._busy then return end
	
	LMapVote._busy=false;

	local count = {};
	for k,v in pairs(LMapVote.Votes)do
		count[v] = (count[v] or 0) + 1;
	end

	local most = {};
	for k,v in pairs(count)do
		local hasMore = 2;
		if most[1] then
			if most[1].count == v then
				hasMore = 1; -- equal
			elseif most[1].count > v then
				hasMore = 0; -- has more.
			end
		end

		if hasMore == 2 then
			most={{map=k,count=v}};
		elseif hasMore == 1 then
			table.insert(most,{map=k});
		end
	end

	local winner;
	if #most < 1 then
		Msg("Could not select winning map!\nPicking random map.\n");
		winner=table.Random(LMapVote.MapSelection);
	else
		winner = ( table.Random(most) )["map"];
	end

	MsgC(Color(255,255,255,255),winner.." won the mapvote!\n");

	net.Start("LMapVote.WinnerSelected");
	net.WriteString(winner);
	net.Broadcast();

	hook.Call("LMapVote.DoFinish",GAMEMODE,winner);

	if winner==game.GetMap() then
		for k,v in pairs(LMapVote.MapSelection)do
			if v == winner then
				table.remove(LMapVote.MapSelection,k);
			end
		end
		LMapVote.SupressChange=true;
	end

	timer.Simple(4,function()
			hook.Call("LMapVote.Finish",GAMEMODE,winner);

			if not LMapVote.SupressChange then
				LMapVote.SupressChange = false;

				game.ConsoleCommand("changelevel "..winner.."\n");
			else
				LMapVote.SupressChange = false;
			end
	end);
end